(function(){
    window.angular
          .module('koos.reisen')
          .controller('IndexContentCtrl', IndexContentCtrl);


            function IndexContentCtrl($scope,deviceDetector) {
              // Checken ob Mobiler Browser
              $scope.mobile = deviceDetector.isMobile();
              $scope.desktop = deviceDetector.isDesktop();
            }
})();
