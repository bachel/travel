function isMobile() {
 return( navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i) ) ;
}

if( isMobile() ) { document.write('<img .../>'); }

else { document.write('<video playsinline autoplay muted loop poster="img/mobilebg.png" class="bgvid"><source src="vid/Insel.mp4" type="video/mp4"><source src="vid/Insel.ogv" type="video/ogg"> <source src="vid/Insel.webm" type="video/webm"> </video>'); }
