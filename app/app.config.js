(function(){
    window.angular
          .module('koss.reisen.config', [
              'ngAnimate',
              'ngAria',
              'ngCookies',
              'ngMessageFormat',
              'ngMessages',
              'ngResource',
              'ngRoute',
              'ngSanitize',
              'ngTouch',
              'angular-toArrayFilter',
              'ngMeta',
              'ng.deviceDetector'
          ])
          .config(config);

          function config($routeProvider,$locationProvider,ngMetaProvider){
                //Set defaults for arbitrary tags
                ngMetaProvider.setDefaultTag('language', 'de');
                ngMetaProvider.setDefaultTag('robots', 'index,follow');
                ngMetaProvider.setDefaultTag('audience', 'alle');
                ngMetaProvider.setDefaultTag('revisit-after', '1 day');
                // Facebook und Twitter
                ngMetaProvider.setDefaultTag('twitteraccount', '@xxxxxx');
                ngMetaProvider.setDefaultTag('fbtwimage', 'img/logo.png');

                $routeProvider
                    .when('/Tina-Koos-Travel',{
                        templateUrl: 'views/index.content.html',
                        controller: 'IndexContentCtrl',
                        meta: {
                                'title': 'Tina Koos Travel',
                                'description': 'Tina Koos Travel'
                            }
                    })
                    .when('/Tina_Koos',{
                        templateUrl: 'views/Tina_Koos.html',
                        meta: {
                          'title': 'Über Mich',
                          'description': 'Über Mich Tina Koos Travel'
                            }
                    })
                    .when('/Reisearten',{
                        templateUrl: 'views/Service.html',
                        meta: {
                                'title': 'Reisearten',
                                'description': 'Reisearten Tina Koos Travel'
                            }
                    })
                    .when('/Kontakt',{
                        templateUrl: 'views/Kontakt.html',
                        meta: {
                                'title': 'Kontakt',
                                'description': 'Kontakt Tina Koos Travel'
                            }
                    })
                    .when('/Datenschutz',{
                        templateUrl: 'views/Datenschutz.html',
                        meta: {
                                'title': 'Datenschutz',
                                'description': 'Datenschutz TinaKoosTravel.de'
                            }
                    })
                    .when('/Impressum',{
                        templateUrl: 'views/Impressum.html',
                        meta: {
                                'title': 'Impressum',
                                'description': 'Impressum TinaKoosTravel.de'
                            }
                    })
                    

                    .otherwise({redirectTo: '/Tina-Koos-Travel'});
                    $locationProvider.html5Mode({
                      enabled: true,
                      requireBase: false
                    });
          }
})();
