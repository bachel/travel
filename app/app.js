(function(){
window.angular
      .module('koos.reisen', [
              'koss.reisen.config'
             ])
      .run(run);
        function run(ngMeta,$rootScope,$location,$window){
            ngMeta.init();
            $rootScope.$on('$routeChangeSuccess',function (event) {
                if (!$window.ga)return;
                $window.ga('send', 'pageview', { page: $location.path() });
            });

            }
})();
